<?php

namespace app\models;

use Exception;
use yii\base\Model;

class Calculator extends Model
{
    public $string;
    public $result;
    public $evalutedString;
    /* разрешаем м использовать математические функции*/
    protected $whitelist = ["abs", "acos", "acosh", "asin", "asinh", "atan2", "atan", "atanh", "base_convert", "bindec", "ceil", "cos", "cosh", "decbin", "dechex", "decoct", "deg2rad", "exp", "expm1", "floor", "fmod", "getrandmax", "hexdec", "hypot", "intdiv", "is_finite", "is_infinite", "is_nan", "lcg_value", "log10", "log1p", "log", "max", "min", "mt_getrandmax", "mt_rand", "mt_srand", "octdec", "pi", "pow", "rad2deg", "rand", "round", "sin", "sinh", "sqrt", "srand", "tan", "tanh"];
    /**
     * @var string
     */
    private $filePath;


    public function exec()
    {
        return $this->_exec();

    }

    public function rules()
    {
        return [
            [['string'], 'required'],
            [['string'], 'ValidateString'],
        ];
    }

    public function ValidateString($attribute, $params, $validator)
    {
        if (preg_match('/.*(echo|die|eval).*/', $this->$attribute, $matches))
            $this->addError($attribute, 'not safe string:' . $matches[1]);/*вызов конструкций запрещен чтобы не  было stdout*/
        if (preg_match('/.*?([A-za-zА-Яа-я]+)\(.*/u', $this->$attribute, $matches) && !in_array($matches[1], $this->whitelist))
            $this->addError($attribute, 'not safe string:' . $matches[1]);/* вызов  функций тоже запрещен (кроме таметатических)*/
        return $this->hasErrors();
    }

    protected function checkSyntax($fileName, $checkIncludes = true)
    {
        // If it is not a file or we can't read it throw an exception
        if (!is_file($fileName) || !is_readable($fileName))
            throw new \Exception("Cannot read file " . $fileName);

        // Sort out the formatting of the filename
        $fileName = realpath($fileName);

        // Get the shell output from the syntax check command
        $output = shell_exec('php -l "' . $fileName . '"');

        // Try to find the parse error text and chop it off
        $syntaxError = preg_replace("/Errors parsing.*$/", "", $output, -1, $count);

        // If the error text above was matched, throw an exception containing the syntax error
        if ($count > 0)
            throw new \Exception(trim($syntaxError));

        // If we are going to check the files includes
        if ($checkIncludes) {
            foreach ($this->getIncludes($fileName) as $include) {
                // Check the syntax for each include
                $this->checkSyntax($include);
            }
        }
    }

    protected function getIncludes($fileName)
    {
        // NOTE that any file coming into this function has already passed the syntax check, so
        // we can assume things like proper line terminations

        $includes = array();
        // Get the directory name of the file so we can prepend it to relative paths
        $dir = dirname($fileName);

        // Split the contents of $fileName about requires and includes
        // We need to slice off the first element since that is the text up to the first include/require
        $requireSplit = array_slice(preg_split('/require|include/i', file_get_contents($fileName)), 1);

        // For each match
        foreach ($requireSplit as $string) {
            // Substring up to the end of the first line, i.e. the line that the require is on
            $string = substr($string, 0, strpos($string, ";"));

            // If the line contains a reference to a variable, then we cannot analyse it
            // so skip this iteration
            if (strpos($string, "$") !== false)
                continue;

            // Split the string about single and double quotes
            $quoteSplit = preg_split('/[\'"]/', $string);

            // The value of the include is the second element of the array
            // Putting this in an if statement enforces the presence of '' or "" somewhere in the include
            // includes with any kind of run-time variable in have been excluded earlier
            // this just leaves includes with constants in, which we can't do much about
            if ($include = $quoteSplit[1]) {
                // If the path is not absolute, add the dir and separator
                // Then call realpath to chop out extra separators
                if (strpos($include, ':') === FALSE)
                    $include = realpath($dir . DIRECTORY_SEPARATOR . $include);

                array_push($includes, $include);
            }
        }

        return $includes;
    }

    protected function _exec()
    {
        try {
            $result = null;
            $this->evalutedString = '$result =(' . $this->string . ');';
//filed  tests see https://github.com/Glavin001/atom-beautify/issues/1108
            $this->filePath = @tempnam("/tmp", "eval" . mt_rand());
            file_put_contents($this->filePath, '<?php' . PHP_EOL . $this->evalutedString . '?>');
            register_shutdown_function('unlink', $this->filePath);
            $this->checkSyntax($this->filePath);
            require($this->filePath);
            $this->result = $result;
            return  $this->result;
        } catch (\Exception $e) {
            return $this->addError('string', 'ошибочное выражение');
        }

    }

    public function generateExamples($left = 0, $right = 10, $min = 1, $max = 25)
    {
        for ($i = $left; $i <= $right; $i++)
            for ($j = $left; $j <= $right; $j++) {
                $a = mt_rand($min, $max);
                $b = mt_rand($min, $max);
                foreach (['+', '-', '/', '*', 'pow', 'fmod', 'max'] as $action) {
                    $str = preg_match('/[a-zA-z]+/', $action) ? ($action . '(' . $a . ',' . $b . ')') : ($a . $action . $b);
                    try {
                        eval('$result =(' . $str . ');');
                    } catch (Exception $e) {
                        $result = $e->getMessage();
                    }
                    $text[] = ['a' => $a, 'b' => $b, 'action' => $action, 'string' => $str, 'result' => $result];
                    $test[] = ' * @example ["' . $str . '", ' . $result . ']';
                }
            }
        return [$text, $test];

    }
}
