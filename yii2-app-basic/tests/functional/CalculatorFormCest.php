<?php

class CalculatorFormCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnPage(['calculator/index']);
    }

    public function openlculatorPage(\FunctionalTester $I)
    {
        $I->see('calculator', 'h1');
    }

    public function submitEmptyForm(\FunctionalTester $I)
    {
        $I->submitForm('.calculate-form form', []);
        $I->expectTo('see validations errors');
        $I->see('calculator', 'h1');
        $I->see('String  cannot be blank');
    }

    /**
     * @example ["die"]
     * @example ["echo"]
     * @example ["var_dmp"]
     * @example ["abracadabr"]
     */
    public function submitFormWithNotSafeString(\FunctionalTester $I, \Codeception\Example $example)
    {
        $I->submitForm('.calculate-form form', [
            'Calculator[string]' => $example[0] . '()'
        ]);
        $I->see('not safe string:' . $example[0]);
    }

    /**
     * @example ["7*10", 70]
     * @example ["pow(7,10)", 282475249]
     * @example ["fmod(7,10)", 7]
     * @example ["intdiv(7,10)", 0]
     * @example ["25+9", 34]
     * @example ["25-9", 16]
     * @example ["25/9", 2.7777777777778]
     * @example ["25*9", 225]
     * @example ["pow(25,9)", 3814697265625]
     * @example ["fmod(25,9)", 7]
     * @example ["intdiv(25,9)", 2]
 */
    public function submitFormSuccessfully(\FunctionalTester $I, \Codeception\Example $example)
    {
        $I->submitForm('.calculate-form form', [
            'Calculator[string]' => $example[0],
        ]);
//$I->seeInSource('[value="3+3"]');
        $I->seeResponseCodeIs(200);
        $I->see($example[1] . '', '.alert .badge');
    }
}
