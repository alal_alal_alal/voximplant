<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calculator */
/* @var $form ActiveForm */


?>
<div class="calculate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'string')->textInput(['id' => 'terminal_input']); ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- calculate-form -->
