<?php


use yii\bootstrap\Alert;
use yii\bootstrap\Collapse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calculator */
/* @var $form ActiveForm */
/* @var $text array */
/* @var $test array */
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/idea.min.css');
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/highlight.min.js');
$this->registerJs(/** @lang JavaScript */ 'document.querySelectorAll(\'.hljs\').forEach((block) => {
    hljs.highlightBlock(block);
});


$(document).on(\'click\', \'ul.example>li\', function (event, el) {
    text = $(this).children(\'span\').data(\'string\')
    $(\'#terminal_input\').val(text)
    
    event.preventDefault();
})
$(\'[data-toggle="popover"]\').popover({
    placement : \'left\',
    trigger: \'hover\',
});

');
?>
    <h1>calculator</h1>

<?php
if ($model->evalutedString)
    echo Alert::widget([
        'options' => [
            'class' => 'alert-success',
        ],
        'body' => Html::tag('code', $model->evalutedString . Html::tag('span', $model->result, ['class' => 'badge'])),
    ]);
echo $this->render('form', ['model' => $model]);
echo Collapse::widget([
    'items' => [
        // equivalent to the above
        [
            'label' => 'задание',
            'content' => '
   <p>На одном из фрэймворков реализовать собственный компонент (Yii2 или Symfony 4+), который:</p>
<ul> <li data-toggle="popover" title="сделано" data-content="\app\models\Calculator::_exec() ">Получает на вход строку, в которой цифры и знаки математических действий.</li>
<li data-toggle="popover" title="сделано" data-content="через  правила в  этой  модели"> Выдает на выходе результат или ошибку, если что-то пошло не так.</li>
 <li  data-toggle="popover" title="сделано" data-content="возможность расширения имеется :) ">Имеет возможность расширения (например, если в какой-то момент будет решено считать через wolfram alpha).</li>
<li   data-toggle="popover" title="сделано" data-html="true" data-content="запустить  <code>composer update</code>  Yii2 скачается с гитхаба">Корректную инициацию для композера.</li>
<li   data-toggle="popover" title="сделано" data-html="true" data-content="работают  Codeception тесты  из   коробки  Yii2+ <code>\CalculatorFormCest</code> который проверяет калькулятор  с раными примерами">Юнит-тесты, покрывающие ключевые моменты в реализации.</li>
<li    data-toggle="popover" title=" не сделано" data-html="true" data-content="на  момент написания (<code>7796e3bec1372ed97e1766f03bdba1f1e744c01b</code>)  в этом комите еще не  сделано потомучто пка не  разобрался как  настраивать  gitlab ci">Конфиг для gitlab ci для прогона на версиях php от 5.3 до 7.2.</li>
</ul>'
        ],
        [
            'label' => 'примеры',
            'content' => Html::ul(\yii\helpers\ArrayHelper::getColumn($text, function ($i) {
                if (gettype($i['result']) == 'string')
                    $class = 'text-danger hljs php';
                else
                    $class = 'hljs php';
                if (preg_match('/[a-zA-z]+/', $i['action'])) /*если функция*/
                    $context = $i['string'] . ' = ' . $i['result'];
                else
                    $context = $i['a'] . ' ' . $i['action'] . ' ' . $i['b'] . ' = ' . $i['result'];
                return
                    Html::tag('span', $context, ['class' => $class, 'data-string' => $i['string']]);
            }), ['encode' => false, 'class' => 'example'])
            ,
            // open its content by default
            //  'contentOptions' => ['class' => 'in']
        ],
        [
            'label' => ' для codeception',
            'content' => Html::ul(ArrayHelper::getColumn($test, function ($i, $index) {
                return
                    Html::tag('span', $i, ['class' => 'hljs php']);
            }), ['encode' => false])
            ,
            // open its content by default
            //  'contentOptions' => ['class' => 'in']
        ],
        [
            'label' => 'тесты',
            'content' => Html::img('img/tests.png'),
            // open its content by default
            //  'contentOptions' => ['class' => 'in']
        ],
    ]
]);
?>

