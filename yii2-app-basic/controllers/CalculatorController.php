<?php

namespace app\controllers;

use app\models\Calculator;

class CalculatorController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model =  new  Calculator();
        if ($model->load(\Yii::$app->request->post()) && $model->validate())
            $model->exec();
        list($text, $test) = $model->generateExamples();
        return $this->render('index', ['model' => $model, 'text' => $text, 'test' => $test]);
    }

}
